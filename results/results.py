#!/usr/bin/env python3
# Copyright (C) Intel Corp.  2018.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *    Clayton Craft <clayton.a.craft@intel.com>
#  *    Mark Janes <mark.a.janes@intel.com>
#  **********************************************************************/
from collections import namedtuple
from flask import escape
from flask import Flask
from flask import make_response
from flask import g
from flask import redirect
from flask import render_template
from flask import request
import MySQLdb
import datetime
import os
import re
import urllib
from functools import wraps

app = Flask(__name__)
secure_cookie = True
# Allow disabling the secure cookie flag (e.g. when running for development)
if 'SESSION_COOKIE_SECURE' in os.environ:
    secure_cookie = os.environ.get('SESSION_COOKIE_SECURE').lower()
    secure_cookie = secure_cookie == 'true' or secure_cookie == '1'
# Flag for indicating whether site is internal or not. Used to hide or
# display certain elements of the site that don't make sense if site is
# not internal
internal_site = False
if 'SITE_IS_INTERNAL' in os.environ:
    internal_site = os.environ.get('SITE_IS_INTERNAL').lower()
    internal_site = internal_site == 'true' or internal_site == '1'


def sort_section(section_dicts, sort_pref):
    """ Sort given section by column/direction
    section_dicts: list of dictionaries (representing rows)
    sort_pref: string in the form of '<column>:<direction>'
    Returns: sorted section_dicts or section_dicts if unable to sort """
    if not section_dicts:
        return []
    if sort_pref and ':' in sort_pref:
        try:
            col, direction = sort_pref.split(':')
        except ValueError:
            return section_dicts
        if col in section_dicts[0]:
            return sorted(section_dicts, key=lambda k: k[col],
                          reverse=direction == 'desc')
    return section_dicts


def apply_new_section_sort(request, sort_prefs, req_path=None):
    """ Save new section sorting preferences to cookie(s)
    request: flask request object
    sort_prefs: dictionary of section sorting prefs
    Returns: flask response with cookies set, or None """
    resp = None
    if not req_path:
        req_path = request.path
    new_sort_col = request.args.get('sort_col')
    new_sort_section = request.args.get('sort_section')
    if new_sort_section in sort_prefs:
        direction = 'asc'
        if sort_prefs[new_sort_section]:
            try:
                old_col, old_direction = sort_prefs[new_sort_section].split(':')
            except ValueError:
                # Unable to unpack old sort preferences, so default to sorting
                # by new col and asc direction
                old_col, old_direction = (new_sort_col, 'asc')
            if new_sort_col == old_col:
                # Use opposite direction if section is already sorted by column
                if old_direction == 'asc':
                    direction = 'desc'
        # Save new sort preferences and redirect to clear querystring
        resp = make_response(
            redirect(request.path +
                     '#' + urllib.parse.quote_plus('%s' % new_sort_section),
                     code=303))
        resp.set_cookie(new_sort_section + '_sort_pref',
                        '%s:%s' % (new_sort_col, direction),
                        expires=(datetime.datetime.now()
                                 + datetime.timedelta(days=9000)),
                        httponly=True, secure=secure_cookie,
                        samesite='Strict')
    return resp


def get_section_sort_prefs(request, sections):
    """ Read section sort preferences from cookies
    request: flask request object
    sections: list of sections on page
    Returns: dictionary of section sorting prefs, or None """
    sort_prefs = {}
    for section in sections:
        cookie = request.cookies.get(section + '_sort_pref')
        if not cookie:
            sort_prefs[section] = ''
        else:
            sort_prefs[section] = cookie
    return sort_prefs


def apply_new_section_expand(request, expand_prefs):
    """ Save new section expand preferences to cookie(s)
    request: flask request object
    expand_prefs: dictionary of section sorting prefs
    Returns: flask response with cookies set, or None """
    resp = None
    section = request.args.get('expand_toggle')
    if section in expand_prefs:
        expand_prefs[section] = expand_prefs[section] is not True
        resp = make_response(
            redirect(request.path
                     + '#' + urllib.parse.quote_plus('%s' % section), code=303))
        resp.set_cookie(section + '_expand', str(expand_prefs[section]),
                        expires=(datetime.datetime.now()
                                 + datetime.timedelta(days=9000)),
                        httponly=True, secure=secure_cookie,
                        samesite='Strict')
    return resp


def get_section_expand_prefs(request, sections):
    """ Read section expand preferences from cookies
    request: flask request object
    sections: list of sections on page
    Returns: dictionary of section expand prefs, or None """
    expand_prefs = {}
    for section in sections:
        cookie = request.cookies.get(section + '_expand')
        if not cookie:
            expand_prefs[section] = False
        else:
            expand_prefs[section] = cookie.lower() == 'true'
    return expand_prefs


def is_valid_database(database, cur):
    """ Searches a tuple of database tuples (i.e. what
    would be returned by cursor.fetchall())  for the given
    database. Returns True if found."""
    # Handle case where the job is not a valid database
    database = database.lower()
    cur.execute("show databases")
    for db in cur.fetchall():
        try:
            if database == db[0].lower():
                return True
        except IndexError:
            continue
    return False


def ServerError(request):
    prev_url = request.referrer or '/'
    return render_template("500.html", url=prev_url), 500


def sql_safe(s):
    """ Determine if the given string is likely to be safe for SQL
    operations """
    banned_strings = ["\'", "\"", ";"]
    for banned in banned_strings:
        if banned in s:
            return False
    return True


@app.before_request
def before_request():
    sql_user = 'jenkins'
    if "SQL_DATABASE_USER" in os.environ:
        sql_user = os.environ["SQL_DATABASE_USER"]

    sql_pw = ''
    if "SQL_DATABASE_PW_FILE" in os.environ:
        sql_pw_file = os.environ["SQL_DATABASE_PW_FILE"]
        if os.path.exists(sql_pw_file):
            with open(sql_pw_file, 'r') as f:
                sql_pw = f.read().rstrip()
    host = "localhost"
    if "SQL_DATABASE_HOST" in os.environ:
        host = os.environ["SQL_DATABASE_HOST"]
    g.db = MySQLdb.connect(host=host, passwd=sql_pw, user=sql_user,
                           use_unicode=True, charset="utf8")
    g.db.set_character_set('utf8')
    g.cur = g.db.cursor()
    g.cur.execute('SET NAMES utf8;')
    g.cur.execute('SET CHARACTER SET utf8;')
    g.cur.execute('SET character_set_connection=utf8;')


@app.after_request
def after_request(response):
    g.db.close()
    return response
            
@app.route("/")
def main():
    jobfilters = []
    jobfilter_str = ''
    # Apply any job filters if included in query string
    if 'jobfilter' in request.args:
        jobfilter_str = request.args.get('jobfilter')
        resp = make_response(redirect(request.path, code=303))
        resp.set_cookie('jobfilter', jobfilter_str,
                        expires=datetime.datetime.now() +
                        datetime.timedelta(days=9000),
                        httponly=True, secure=secure_cookie,
                        samesite='Strict')
        return resp

    # Get sort preferences, save preferences if new ones are passed by
    # query string
    sections = ['jobs']
    sort_prefs = get_section_sort_prefs(request, sections)
    resp = apply_new_section_sort(request, sort_prefs)
    # If new preferences were given, reload/redirect to apply
    if resp:
        return resp

    # Get job filter preference
    jobfilter_str = request.cookies.get('jobfilter')
    if jobfilter_str:
        # Delimiter for terms is whitespace
        jobfilters = jobfilter_str.split()

    g.cur.execute("show databases")
    jobs = g.cur.fetchall()
    job_dicts = []
    for j in jobs:
        j = str(j[0])
        if j not in ["performance_schema", "information_schema", "mysql"]:
            # get latest build and determine if there are any failures
            # j is already a valid database since it came from 'show databases'
            g.cur.execute("use " + j)
            g.cur.execute("select build_id, build_name, end_time from build "
                          "where imported=TRUE order by build_id DESC limit 1")
            try:
                (last_build_id, last_build_name,
                 last_build_time) = g.cur.fetchone()
            except TypeError:
                # we get here if a new build is being imported when the
                # page is hit
                continue
            g.cur.execute("select component.status from component join "
                          "build on component.build_id=build.build_id where "
                          "component.build_id=%s and build.imported=TRUE",
                          [last_build_id])
            status = 'success'
            for c in g.cur.fetchall():
                s = c[0]
                if s == 'failure' or s == 'aborted':
                    status = 'failure'
                    break
                elif s == 'unstable':
                    status = 'unstable'
            last_fail_name = ''
            last_fail_id = ''
            last_fail_count = ''
            last_fail_time = ''
            # Gather info on last failed build for job
            g.cur.execute("select build_id, build_name, fail_count, end_time "
                          "from build where fail_count > 0 and imported=TRUE "
                          "order by build_id  desc limit 1")
            try:
                (last_fail_id, last_fail_name, last_fail_count,
                 last_fail_time) = g.cur.fetchone()
            except TypeError:
                pass
            job_dicts.append({
                'name': j,
                'status': status,
                'last_build_id': str(last_build_id),
                'last_build_name': last_build_name,
                'last_build_time': str(last_build_time),
                'last_failed_build_id': str(last_fail_id),
                'last_failed_build_name': last_fail_name,
                'last_failed_build_count': str(last_fail_count),
                'last_failed_build_time': str(last_fail_time),
            })

    # filter jobs based on user preference
    filtered_job_dicts = []
    if jobfilters:
        for jobfilter in jobfilters:
            if jobfilter is None:
                continue
            for job in job_dicts:
                if jobfilter in job['name'] and job not in filtered_job_dicts:
                    filtered_job_dicts.append(job)
        job_dicts = filtered_job_dicts

    # sort sections on page
    job_dicts = sort_section(job_dicts, sort_prefs['jobs'])
    return render_template('main.html',
                           jobs=job_dicts,
                           top_links=[{"text": "i965 Mesa CI",
                                       "href": "."}],
                           jobfilter=jobfilter_str, sort_prefs=sort_prefs)

@app.route("/<job>/builds/")
def list_builds(job):
    # Get sort preferences, save preferences if new ones are passed by
    # query string
    sections = ['builds']
    sort_prefs = get_section_sort_prefs(request, sections)
    resp = apply_new_section_sort(request, sort_prefs)
    # If new preferences were given, reload/redirect to apply
    if resp:
        return resp

    if not is_valid_database(job, g.cur):
        return ServerError(request)

    g.cur.execute("use " + job)

    g.cur.execute("select build_id, build_name, pass_count, "
                  "filtered_pass_count, fail_count, end_time from build "
                  "where imported=TRUE")
    builds = []
    for b in g.cur.fetchall():
        g.cur.execute("select status from component where "
                      "build_id=%s", [b[0]])
        status = 'success'
        for c in g.cur.fetchall():
            s = c[0]
            if s == 'failure' or s == 'aborted':
                status = 'failure'
                break
            elif s == 'unstable':
                status = 'unstable'

        builds.append({"build_id" : b[0],
                       "build_name" : b[1],
                       "pass_count" : b[2],
                       "filtered_pass_count" : b[3],
                       "fail_count" : b[4],
                       "end_time" : b[5],
                       "status": status
                       })
    # Sort sections on page
    builds = sort_section(builds, sort_prefs['builds'])
    return render_template('builds.html', job=job, builds=builds,
                           top_links=[{"text": "i965 Mesa CI",
                                       "href": "../.."},
                                      {"text": job,
                                       "href": "."}],
                           title='Builds for ' + job,
                           sort_prefs=sort_prefs)


@app.route("/<job>/builds/<build_id>/group/<group_id>/<component_id>/artifacts/<artifact_id>")
def display_artifact(job, build_id, group_id, component_id, artifact_id):
    # Throw error page if passed an invalid build ID
    try:
        int(build_id)
        int(component_id)
        int(artifact_id)
    except ValueError:
        return ServerError(request)
    # Do not attempt to use parameters that are not safe for sql
    for param in [job, build_id, group_id, component_id, artifact_id]:
        if not sql_safe(param):
            return ServerError(request)
    if not is_valid_database(job, g.cur):
        return ServerError(request)

    g.cur.execute("use " + job)

    g.cur.execute("select filename, type, data from artifact where "
                  "component_id=%s and artifact_id=%s",
                  [component_id, artifact_id])
    try:
        artifact = g.cur.fetchone()
    except TypeError:
        return ServerError(request)

    artifact_dict = {
        'filename': artifact[0],
        'type': artifact[1],
        'data': escape(artifact[2]),
    }
    g.cur.execute("select component_name, machine, arch, shard from component "
                  "where component_id=%s", [component_id])
    component = g.cur.fetchone()
    component_formatted_name = ' / '.join(component)
    g.cur.execute("select build_name from build where build_id=%s", [build_id])
    try:
        build_name = g.cur.fetchone()[0]
    except TypeError:
        # No valid build found (i.e. link is stale and pointing to a build that
        # was purged)
        return ServerError(request)
    top_links = [{"text": "i965 Mesa CI",
                  "href": "/"},
                 {"text": job,
                  "href": "/" + job + "/builds"},
                 {"text": build_name,
                  "href": "../../../../group/63a9f0ea7bb98050796b649e85481845"}]

    return render_template('artifact.html', job=job, artifact=artifact_dict,
                           component_name=component_formatted_name,
                           top_links=top_links)

@app.route("/<job>/builds/<build_id>/group/<group_id>")
def list_fails(job, build_id, group_id):
    # Throw error page if passed an invalid build ID
    try:
        int(build_id)
    except ValueError:
        return ServerError(request)

    # Get sort and expand preferences, save preferences if new ones are
    # passed by query string
    sections = ['components', 'subgroups', 'test_results', 'fails',
                'revisions']
    sort_prefs = get_section_sort_prefs(request, sections)
    resp = apply_new_section_sort(request, sort_prefs)
    # If new preferences were given, reload/redirect to apply
    if resp:
        return resp
    expand_prefs = get_section_expand_prefs(request, sections)
    resp = apply_new_section_expand(request, expand_prefs)
    # If new preferences were given, reload/redirect to apply
    if resp:
        return resp

    # Do not attempt to use parameters that are not safe for sql
    for param in [job, build_id, build_id, group_id]:
        if not sql_safe(param):
            return ServerError(request)
    if not is_valid_database(job, g.cur):
        return ServerError(request)

    g.cur.execute("use " + job)
    g.cur.execute("select build_name from build where build_id=%s", [build_id])
    try:
        build_name = g.cur.fetchone()[0]
    except TypeError:
        # No valid build found (i.e. link is stale and pointing to a build that
        # was purged)
        return ServerError(request)
    g.cur.execute("select test_name from test where test_id=%s", [group_id])
    try:
        group_name = g.cur.fetchone()[0]
    except TypeError:
        return ServerError(request)
    g.cur.execute("select result_id, test_name, hardware, arch, "
                  "status, filtered_status, time, test_id "
                  "from result join test using (test_id) "
                  """where (filtered_status="fail" and build_id=%s) """
                  "limit 1000", [build_id])
    fail_list = g.cur.fetchall()
    fail_dicts = []
    for f in fail_list:
        if f[1].startswith(group_name) or group_name == "root":
            fail_dicts.append({
                'result_id': f[0],
                'test_name': f[1],
                'hardware': f[2],
                'arch': f[3],
                'status': f[4],
                'filtered_status': f[5],
                'time': f[6],
                'test_id': f[7],
            })
    g.cur.execute("select test_name, test_id from parent "
                  "join test using(test_id) "
                  "where parent_id=%s "
                  "order by test_name", [group_id])
    subgroups = g.cur.fetchall()

    statistics = []
    subgroup_dicts = []
    for subgroup in subgroups:
        g.cur.execute("select pass_count, fail_count, filtered_pass_count, time from group_ "
                      """where build_id=%s and test_id=%s""",
                      [build_id, subgroup[1]])
        statistics = g.cur.fetchone()
        if not statistics:
            continue
        (pass_count, fail_count, filtered_pass_count, t_time) = statistics
        subgroup_dicts.append({"subgroup_name" : subgroup[0],
                               "subgroup_id" : subgroup[1],
                               "pass_count" : pass_count,
                               "fail_count" : fail_count,
                               "filtered_pass_count" : filtered_pass_count,
                               "time" : str(datetime.timedelta(seconds=float(t_time)))})
        
    g.cur.execute("select result_id, test_name, hardware, arch, "
                  "status, filtered_status "
                  "from result join test using (test_id) "
                  "where (test_id=%s and build_id=%s) ", [group_id, build_id])
    test_results = g.cur.fetchall()
    test_dicts = [{"result_id" : result[0],
                   "test_name" : result[1],
                   "hardware" : result[2],
                   "arch" : result[3],
                   "status": result[4],
                   "filtered_status" : result[5] } for result in test_results]

    top_links = [{"text": "i965 Mesa CI",
                  "href": "../../../.."},
                 {"text": job,
                  "href": "../../../builds/"},
                 {"text": build_name,
                  "href": "63a9f0ea7bb98050796b649e85481845"}]
    uplink = ""
    components_dicts = []
    base_url = ""
    component_failures = False
    if group_name != "root":
        for group in group_name.split("."):
            uplink += group
            g.cur.execute("select test_id from test where test_name=%s", [uplink])
            top_links.append({"text": group, "href": g.cur.fetchone()[0]})
            uplink += "."
    else:
        g.cur.execute("select arch, component_name, shard, machine, "
                      "component.start_time, component.end_time, "
                      "component.build_id, build, url, component_id, "
                      "status from component inner join build on "
                      "component.build_id=build.build_id where "
                      "component.build_id=%s", [build_id])
        components = g.cur.fetchall()
        for component in components:
            base_url = component[8].split('job')[0]
            component_id = str(component[9])
            end_time = component[5] if component[5] else component[4]
            g.cur.execute("select filename, type, artifact_id from artifact "
                          "where component_id=%s", [component_id])
            artifacts = g.cur.fetchall()
            artifacts_dict = []
            for artifact in artifacts:
                artifacts_dict.append({
                    'filename': artifact[0],
                    'type': artifact[1],
                    'id': str(artifact[2]),
                })
            status_severity = 0
            if component[10] == 'unstable':
                status_severity = 1
            elif component[10] == 'failure':
                status_severity = 2
            elif component[10] == 'aborted':
                status_severity = 2
            components_dicts.append({
                'arch': component[0],
                'component_name': component[1],
                'shard': component[2],
                'machine': component[3],
                'start_time': component[4].strftime('%Y-%m-%d %H:%M:%S'),
                'end_time': end_time.strftime('%Y-%m-%d %H:%M:%S'),
                'build_id': component[6],
                'build': component[7],
                # construct <http://site.com/jobs>/Leeroy/<build>
                'component_url':  (base_url + 'job/Leeroy/'
                                   + str(component[7])),
                'machine_url': (base_url + 'computer/' + component[3]
                                + '/builds'),
                'component_id': component_id,
                'artifacts': artifacts_dict,
                'status': component[10],
                'status_severity': status_severity
            })
            if 'failure' == component[10] or 'aborted' == component[10]:
                component_failures = True
    # Sort sections on page
    # If there are component failures, move them to the top of the table
    # (overriding user sort preference for table)
    if component_failures:
        components_dicts = sort_section(components_dicts,
                                        'status_severity:desc')
        # Auto-expand components table when there are failures
        expand_prefs['components'] = True
    else:
        components_dicts = sort_section(components_dicts,
                                        sort_prefs['components'])
    subgroup_dicts = sort_section(subgroup_dicts, sort_prefs['subgroups'])
    test_dicts = sort_section(test_dicts, sort_prefs['test_results'])
    fail_dicts = sort_section(fail_dicts, sort_prefs['fails'])

    # If on build root page, send result_path and revisions for
    # bisect/accept/retest buttons
    result_path = None
    # jinja's limited logic functionality forces us to create
    # two lists here to pass to the template
    revisions_list = []
    revisions_tuples = []
    Revision = namedtuple('Revision', 'project sha description author changed')
    if components_dicts:

        g.cur.execute("select result_path from build where build_id=%s",
                      [build_id])
        result_path = g.cur.fetchone()[0]

        g.cur.execute("select project, commit, author, description, sha from "
                      "revision where build_id=%s", [build_id])
        for project in g.cur.fetchall():
            name, commit, author, description, sha = project
            changed = False
            # Compare revision shas with previous build to detect changes
            g.cur.execute("select sha from revision where build_id < %s "
                          "and project=%s order by build_id desc limit 1",
                          [build_id, name])
            last_sha = g.cur.fetchone()
            if last_sha and last_sha[0] != sha:
                changed = True
            revisions_tuples.append(Revision(project=name, sha=sha,
                                             author=author,
                                             description=description,
                                             changed=changed))
            revisions_list.append(commit)
        revisions_tuples = sorted(revisions_tuples, key=lambda x: x.project)

    # Select all builds for this job (except current build), this
    # is used for comparing this build to other builds
    builds = {}
    g.cur.execute("select build_id, build_name from build where not "
                  "build_id=%s and imported=TRUE order by build_id desc",
                  [build_id])
    for build in g.cur.fetchall():
        builds[str(build[0])] = build[1]

    g.cur.execute("select imported from build where build_id=%s", [build_id])
    imported = bool(g.cur.fetchone()[0])
    return(render_template('results.html', top_links=top_links,
                           job=job, build_name=build_name,
                           build_id=build_id, group_id=group_id,
                           group_name=group_name,
                           fails=fail_dicts,
                           subgroups=subgroup_dicts,
                           test_results=test_dicts,
                           components=components_dicts,
                           revisions_tuples=revisions_tuples,
                           revisions_list=revisions_list,
                           result_path=result_path,
                           base_url=base_url,
                           expanded_sections=expand_prefs,
                           internal_site=internal_site, title=(job + " "
                                                               + build_name
                                                               + "/"
                                                               + group_name),
                           builds=builds, sort_prefs=sort_prefs,
                           imported=imported))


@app.route("/<job>/compare")
def compare(job):
    if 'lhs' in request.args.keys():
        lhs = request.args.get('lhs')
    if 'rhs' in request.args.keys():
        rhs = request.args.get('rhs')
    if not lhs or not rhs:
        return ServerError(request)
    try:
        int(lhs)
        int(rhs)
    except ValueError:
        return ServerError(request)
    # Get sort and expand preferences, save preferences if new ones are
    # passed by query string
    sections = ['test_compare']
    sort_prefs = get_section_sort_prefs(request, sections)
    resp = apply_new_section_sort(request, sort_prefs,
                                  "{}?lhs={}&rhs={}".format(request.path, lhs,
                                                            rhs))
    # If new preferences were given, reload/redirect to apply
    if resp:
        return resp

    # Do not attempt to use parameters that are not safe for sql
    for param in [job, lhs, rhs]:
        if not sql_safe(param):
            return ServerError(request)
    if not is_valid_database(job, g.cur):
        return ServerError(request)

    g.cur.execute("use " + job)

    g.cur.execute("select build_name from build where build_id=%s limit 1",
                  [lhs])
    try:
        lhs_name = g.cur.fetchone()[0]
    except TypeError:
        return ServerError(request)
    g.cur.execute("select build_name from build where build_id=%s limit 1",
                  [rhs])
    try:
        rhs_name = g.cur.fetchone()[0]
    except TypeError:
        return ServerError(request)

    top_links = [{"text": "i965 Mesa CI",
                  "href": "../../../.."},
                 {"text": job,
                  "href": "./builds/"}]

    # Diff method:
    # - select results in lhs build that have different status than in
    #   rhs build
    # - do the same for rhs result build
    # - get remaining statuses for tests missing in lhs list that are in
    #   rhs list
    # - do the same for rhs result list

    # This select is called twice, once for lhs build, once for rhs build
    get_fails_query = ("select LHS.result_id, LHS.test_id, LHS.arch, "
                       "LHS.status, LHS.hardware, test.test_name, "
                       "RHS.result_id, RHS.status from result LHS join test "
                       "on LHS.test_id=test.test_id left join result RHS on "
                       "LHS.test_id=RHS.test_id and RHS.build_id={} and "
                       "LHS.arch=RHS.arch and RHS.hardware=LHS.hardware "
                       "where LHS.status='fail' and LHS.build_id={} and "
                       "(LHS.status<>RHS.status or RHS.status is null)")
    g.cur.execute(get_fails_query.format(rhs, lhs))
    lhs_fails = g.cur.fetchall()
    g.cur.execute(get_fails_query.format(lhs, rhs))
    rhs_fails = g.cur.fetchall()
    tmp_results = []
    result_dicts = []
    for fail in lhs_fails:
        tmp_results.append({'lhs_result_id': str(fail[0]),
                            'rhs_result_id': None,
                            'test_id': fail[1],
                            'arch': fail[2],
                            'lhs_status': fail[3],
                            'rhs_status': None,
                            'hardware': fail[4],
                            'name': fail[5]})
    for fail in rhs_fails:
        tmp_results.append({'rhs_result_id': str(fail[0]),
                            'lhs_result_id': None,
                            'test_id': fail[1],
                            'arch': fail[2],
                            'rhs_status': fail[3],
                            'lhs_status': None,
                            'hardware': fail[4],
                            'name': fail[5]})

    # Get status and result id for changed tests in both lhs and rhs if
    # they don't exist yet
    for result in tmp_results:
        tmp_result = {}
        # RHS test info needed
        if not result['rhs_result_id']:
            g.cur.execute("select result_id, status from result where "
                          "test_id=%s and hardware=%s and arch=%s "
                          "and build_id=%s", [result['test_id'],
                                              result['hardware'],
                                              result['arch'], rhs])
            # copy test info, modify result ID and status if necessary
            try:
                result_id, status = g.cur.fetchone()
            except (IndexError, TypeError):
                result_id = None
                status = "not run"
            tmp_result = {'name': result['name'],
                          'hardware': result['hardware'],
                          'lhs_status': result['lhs_status'],
                          'lhs_result_id': result['lhs_result_id'],
                          'arch': result['arch'],
                          'test_id': result['test_id']}
            tmp_result.update({'rhs_status': status,
                               'rhs_result_id': str(result_id)})

        # LHS test info needed
        if not result['lhs_result_id']:
            g.cur.execute("select result_id, status from result where "
                          "test_id=%s and hardware=%s and arch=%s "
                          "and build_id=%s", [result['test_id'],
                                              result['hardware'],
                                              result['arch'], lhs])
            # copy test info, modify result ID and status if necessary
            try:
                result_id, status = g.cur.fetchone()
            except (IndexError, TypeError):
                result_id = None
                status = "not run"
            tmp_result = {'name': result['name'],
                          'hardware': result['hardware'],
                          'rhs_status': result['rhs_status'],
                          'rhs_result_id': result['rhs_result_id'],
                          'arch': result['arch'],
                          'test_id': result['test_id']}
            tmp_result.update({'lhs_status': status,
                               'lhs_result_id': str(result_id)})
        if tmp_result:
            result_dicts.append(tmp_result)

    result_dicts = sort_section(result_dicts, sort_prefs['test_compare'])
    return(render_template('compare.html', top_links=top_links,
                           job=job, lhs_name=lhs_name, rhs_name=rhs_name,
                           lhs_id=lhs, rhs_id=rhs, results=result_dicts,
                           sort_prefs=sort_prefs))


@app.route("/<job>/builds/<build_id>/results/<result_id>")
def result(job, build_id, result_id):
    # Throw error page if passed an invalid build ID
    try:
        int(build_id)
        int(result_id)
    except ValueError:
        return ServerError(request)
    # Do not attempt to use parameters that are not safe for sql
    for param in [job, build_id, result_id]:
        if not sql_safe(param):
            return ServerError(request)
    if not is_valid_database(job, g.cur):
        return ServerError(request)

    g.cur.execute("use " + job)

    g.cur.execute("select build_name from build where build_id=%s", [build_id])
    try:
        build_name = g.cur.fetchone()[0]
    except TypeError:
        # No valid build found (i.e. link is stale and pointing to a build that
        # was purged)
        return ServerError(request)
    g.cur.execute("""select test_id, test_name, hardware, arch, """
                  """status, filtered_status, time, stdout, stderr """
                  """from result join test using (test_id) """
                  """where (result_id=%s)""", [result_id])
    result_list = g.cur.fetchone()
    top_links = [{"text": "i965 Mesa CI",
                  "href": "../../../.."},
                 {"text": job,
                  "href": "../../../builds/"},
                 {"text": build_name,
                  "href": "../group/63a9f0ea7bb98050796b649e85481845"}]
    test_name = result_list[1]
    result = {"test_id" : result_list[0],
              "test_name" : result_list[1],
              "hardware" : result_list[2],
              "arch" : result_list[3],
              "status" : result_list[4],
              "filtered_status" : result_list[5],
              "time" : result_list[6],
              "stdout" : result_list[7],
              "stderr" : result_list[8]}
    uplink = ""
    for group in test_name.split("."):
        uplink += group
        g.cur.execute("select test_id from test where test_name=%s", [uplink])
        top_links.append({"text": group, "href": "../group/" + g.cur.fetchone()[0]})
        uplink += "."
    return render_template('test.html', job=job, build_name=build_name,
                           result=result, top_links=top_links,
                           title=(job + " " + build_name + "/"
                                  + result['test_name']))

@app.route("/<job>/test/<test_id>/history")
def history(job, test_id):
    # Get sort preferences, save preferences if new ones are passed by
    # query string
    sections = ['results']
    sort_prefs = get_section_sort_prefs(request, sections)
    resp = apply_new_section_sort(request, sort_prefs)
    # If new preferences were given, reload/redirect to apply
    if resp:
        return resp
    # Do not attempt to use parameters that are not safe for sql
    for param in [job, test_id]:
        if not sql_safe(param):
            return ServerError(request)
    if not is_valid_database(job, g.cur):
        return ServerError(request)

    g.cur.execute("use " + job)

    g.cur.execute("""select test_name from test where test_id=%s """,
                  [test_id])
    test_name = g.cur.fetchone()[0]
    g.cur.execute("""select build_id, build_name, result_id, """
                  """hardware, arch, status, filtered_status, time """
                  """from result join build using (build_id) """
                  """where (test_id=%s) """
                  """order by build_id""", [test_id])
    results = []
    for result in g.cur.fetchall():
        results.append({"build_id" : result[0],
                        "build_name" : result[1],
                        "result_id" : result[2],
                        "hardware" : result[3],
                        "arch" : result[4],
                        "status" : result[5],
                        "filtered_status" : result[6],
                        "time" : result[7]})
    top_links = [{"text": "i965 Mesa CI",
                  "href": "../../../.."},
                 {"text": job,
                  "href": "../../builds/"}]
    # Sort sections on page
    results = sort_section(results, sort_prefs['results'])
    return render_template("history.html", job=job, test_name=test_name,
                           results=results, top_links=top_links,
                           title=(job + "/" + test_name + " History"),
                           sort_prefs=sort_prefs)


@app.route("/<job>/builds/<build_id>/group/<group_id>/search")
def test_search_group(job, build_id, group_id):
    search_limit = 250
    search_step = 0
    search_lower = 0
    # Throw error page if passed an invalid build ID
    try:
        int(build_id)
    except ValueError:
        return ServerError(request)

    # Do not attempt to use parameters that are not safe for sql
    for param in [job, build_id, group_id]:
        if not sql_safe(param):
            return ServerError(request)
    if not is_valid_database(job, g.cur):
        return ServerError(request)

    if 'q' in request.args:
        query = request.args.get('q')
    else:
        return ServerError(request)
    if not sql_safe(query):
        return ServerError(request)
    if 'page' in request.args:
        try:
            search_step = int(request.args.get('page'))
        except ValueError:
            return ServerError(request)
    if search_step:
        search_lower = search_step * search_limit
    g.cur.execute("use " + job)
    g.cur.execute("select test_name from test where test_id=%s", [group_id])
    group_name = g.cur.fetchone()[0]
    prepend_query = group_name
    if group_name == "root":
        prepend_query = ""
    # build query with .format since 'safe' c-style string formatting
    # with the database api breaks the query
    g.cur.execute("""select test_name, test_id from test """
                  """where test_name like '{}%{}%' """
                  """order by test_name limit {},{};""".format(prepend_query,
                                                               query,
                                                               search_lower,
                                                               search_limit))
    results = []
    for result in g.cur.fetchall():
        results.append({"test_name" : result[0],
                        "test_id" : result[1]})
    g.cur.execute("select build_name from build where build_id=%s", [build_id])
    try:
        build_name = g.cur.fetchone()[0]
    except TypeError:
        # No valid build found (i.e. link is stale and pointing to a build that
        # was purged)
        return ServerError(request)

    top_links = [{"text": "i965 Mesa CI",
                  "href": "../../../../.."},
                 {"text": job,
                  "href": "../../../../builds/"},
                 {"text": build_name,
                  "href": "../63a9f0ea7bb98050796b649e85481845"}]
    uplink = ""
    if group_name != "root":
        for group in group_name.split("."):
            uplink += group
            g.cur.execute("select test_id from test where test_name=%s",
                          [uplink])
            top_links.append({"text": group, "href": "../" + g.cur.fetchone()[0]})
            uplink += "."
    search_step_next = search_step + 1
    search_step_prev = search_step - 1 if search_step > 0 else 0
    return render_template("search.html", job=job, test_string=query,
                           build_id=build_id, results=results,
                           top_links=top_links,
                           next_grp=str(search_step_next),
                           prev_grp=str(search_step_prev),
                           title="Tests matching for " + query)
