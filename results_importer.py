#!/usr/bin/env python3
# Copyright (C) Intel Corp.  2018.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *    Clayton Craft <clayton.a.craft@intel.com>
#  *    Mark Janes <mark.a.janes@intel.com>
#  **********************************************************************/

import datetime
import glob
import hashlib
import inotify.adapters
import json
import MySQLdb
import os
import shutil
import subprocess
import tempfile
import xml.etree.cElementTree as et
import warnings


max_builds = {
    'default':              20,
    'mesa_master':          150,
    'vulkancts':            150,
    'mesa_master_daily':    75,
    'vulkancts_daily':      75,
    'mesa_custom':          75,
}


def md5(s):
    """ Return the md5 hash of the given str """
    m = hashlib.md5()
    m.update(s.encode())
    return m.hexdigest()


def q(s, escape=False):
    if not s:
        s = 'NULL'
    if escape and len(s):
        s = MySQLdb.escape_string(s)
    try:
        return '"' + s + '"'
    except TypeError:
        return '"' + s.decode() + '"'


class Tarball:
    def __init__(self, tarfile):
        self.tarfile = tarfile

    def __enter__(self):
        self.tmpdir = tempfile.mkdtemp()
        return self

    def __exit__(self, type, value, traceback):
        os.remove(self.tarfile)
        shutil.rmtree(self.tmpdir)

    def process(self):
        self.extract()
        build_info = self.get_build_info(self.tmpdir + '/build_info.json')
        if not build_info:
            raise RuntimeError("ERR: Unable to process build_info for tar "
                               "file: %s" % self.tarfile)
        return build_info, self.tmpdir

    def extract(self):
        try:
            subprocess.check_output(['tar', 'xf', self.tarfile,
                                     '-C', self.tmpdir])
        except subprocess.CalledProcessError:
            raise RuntimeError("WARN: Unable to extract tar file: %s"
                               % self.tarfile)

    def get_build_info(self, build_info_file):
        build_info = None
        try:
            with open(build_info_file, 'r') as f:
                build_info = json.load(f)
        except FileNotFoundError:
            print("WARN: build_info json not found: %s" % build_info_file)
        return build_info


class ResultsImporter:
    def __init__(self, watch_dir, sql_socket=None, sql_user='jenkins',
                 sql_host='localhost'):
        self.sql_pw = ''
        self.sql_user = sql_user
        self.sql_host = sql_host
        self.sql_socket = sql_socket
        self.watch_dir = watch_dir
        if "SQL_DATABASE_PW_FILE" in os.environ:
            sql_pw_file = os.environ["SQL_DATABASE_PW_FILE"]
            if os.path.exists(sql_pw_file):
                with open(sql_pw_file, 'r') as f:
                    self.sql_pw = f.read().rstrip()
        self.inot = inotify.adapters.Inotify()
        self.inot.add_watch(self.watch_dir)

    def poll(self):
        """ Blocks until a new tarball is found by inotify, then
        returns the path when one is found """
        for event in self.inot.event_gen(yield_nones=False):
            (_, type_names, path, filename) = event
            new_file = path + "/" + filename
            # Only proceed if the event was close after write
            if 'IN_CLOSE_WRITE' not in type_names:
                continue
            if not new_file or not os.path.exists(new_file):
                continue
            return new_file

    def consume(self, tarfile):
        """ Import data in given tarfile """
        with Tarball(tarfile) as tarball:
            try:
                self.build_info, self.resultdir = tarball.process()
            except RuntimeError:
                print("Skipping broken tar...")
                return
            print("Importing build: %s" % (self.build_info["name"]))
            with ImportSql(host=self.sql_host, user=self.sql_user,
                           password=self.sql_pw, socket=self.sql_socket,
                           resultdir=self.resultdir,
                           start_time=self.build_info['start_time'],
                           end_time=self.build_info['end_time'],
                           url=self.build_info['url'],
                           build_num=self.build_info["build"],
                           build_name=self.build_info["name"],
                           job=self.build_info["job"],
                           result_path=self.build_info["result_path"]) as importer:
                importer.add_revisions(self.build_info["revisions"])
                for component in self.build_info['components']:
                    importer.add_component(component)
                importer.prune()
        print("Done processing tarball: %s" % tarfile)


class Test:
    def __init__(self, etree):
        test_name = etree.attrib["name"]
        if "classname" in etree.attrib:
            test_name = ".".join([etree.attrib["classname"],
                                  etree.attrib["name"]])
        name_components = test_name.split(".")
        platform = name_components[-1]
        self.arch = platform[-3:]
        self.hardware = platform[:-3]
        self.name = ".".join(name_components[:-1])
        self._id = md5(self.name)

        # status attribute stores the test result as produced by the suite
        if "status" in etree.attrib:
            self.status = etree.attrib["status"]
        else:
            self.status = "pass"
        if self.status == "crash":
            self.status = "fail"
        self.filtered_status = self.status

        # mesa ci overwrites fail/skip/error tags base on test status
        # in config files
        if etree.find("failure") is not None:
            self.filtered_status = "fail"
        elif etree.find("error") is not None:
            self.filtered_status = "fail"
        elif etree.find("skipped") is not None:
            self.filtered_status = "skip"
        else:
            self.filtered_status = "pass"
        self._stderr = ""
        err = etree.find("system-err")
        if err is not None and err.text is not None:
            self._stderr = err.text
        self._stdout = ""
        out = etree.find("system-out")
        if out is not None and out.text is not None:
            self._stdout = out.text

        self.time = 0.0
        if "time" in etree.attrib:
            self.time = float(etree.attrib["time"])

    def result_row(self, out_rows):
        out_rows.append((self._id, self.hardware, self.arch,
                         self.status, self.filtered_status, self.time,
                         self._stdout, self._stderr))


class TestTree:
    def __init__(self, groupname="root"):
        self._groupname = groupname
        self._group_id = md5(groupname)

        # key is final test component (name.hwarch), value is Test object
        self._tests = {}
        self._subgroups = {}
        self._time = 0.0
        self._pass_count = 0
        self._filtered_pass_count = 0
        self._fail_count = 0

    def parse(self, xml):
        x = et.parse(xml)
        for test_tag in x.findall(".//testcase"):
            t = Test(test_tag)
            self.add_test(t)

    def add_test(self, test):
        test_path = test.name.split(".")
        subgroup = test_path[0]
        sub_test_path = test_path[1:]
        if subgroup not in self._subgroups:
            self._subgroups[subgroup] = TestTree(subgroup)
        self._subgroups[subgroup]._add_test(sub_test_path, test)
        self._time += test.time
        if test.filtered_status == "pass":
            self._pass_count += 1
        if test.filtered_status == "fail":
            self._fail_count += 1
        if test.filtered_status == "skip":
            self._filtered_pass_count += 1

    def _add_test(self, test_path, test):
        if test_path:
            subgroup = test_path[0]
            sub_test_path = test_path[1:]
            if subgroup not in self._subgroups:
                full_group = self._groupname + "." + subgroup
                self._subgroups[subgroup] = TestTree(full_group)
            self._subgroups[subgroup]._add_test(sub_test_path, test)
        else:
            self._tests[test.hardware + test.arch] = test
        self._time += test.time
        if test.filtered_status == "pass":
            self._pass_count += 1
        if test.filtered_status == "fail":
            self._fail_count += 1
        if test.filtered_status == "skip":
            self._filtered_pass_count += 1

    def group_rows(self, _out_rows=None):
        out_rows = []
        if _out_rows is not None:
            out_rows = _out_rows

        out_rows.append((self._group_id, self._pass_count, self._fail_count,
                         self._filtered_pass_count, self._time))
        for (_, subgroup) in self._subgroups.items():
            subgroup.group_rows(out_rows)

        if _out_rows is None:
            return out_rows

    def result_rows(self, _out_rows=None):
        out_rows = []
        if _out_rows is not None:
            out_rows = _out_rows
        for (_, t) in self._tests.items():
            t.result_row(out_rows)
        for (_, subgroup) in self._subgroups.items():
            subgroup.result_rows(out_rows)

        if _out_rows is None:
            return out_rows

    def list_all_tests(self, cursor, _tests=None):
        tests = {}
        if _tests is not None:
            tests = _tests

        # query db to see if any test ids must be added
        # accessing __len__ directly is MUCH faster than len(dict)
        current_count = tests.__len__()
        for (_, subgroup) in self._subgroups.items():
            subgroup.list_all_tests(cursor, tests)

        if self._subgroups and tests.__len__() == current_count:
            # adding any of the subgroups would have also added this
            # group.
            return
        tests[self._group_id] = self._groupname
        if _tests is None:
            return tests

    def new_tests(self, cursor):
        all_tests = self.list_all_tests(cursor)
        known_tests = set()
        all_test_ids = ['\'{}\''.format(a) for a in all_tests]
        step = 1000
        # accessing __len__ directly is MUCH faster than len(dict)
        for i in range(0, all_tests.__len__(), step):
            cmd = ('select test_name from test where test_id=' +
                   '{}'.format(' or test_id='.join(all_test_ids[i:i+step])))
            cursor.execute(cmd)
            known_tests.update([t[0] for t in cursor.fetchall()])
        return [t for t in all_tests.values() if t not in known_tests]

    def test_rows(self, tests):
        out_rows = []
        for t in tests:
            out_rows.append((md5(t), t))
        return out_rows

    def parent_rows(self, tests, _out_rows=None):
        out_rows = []
        if _out_rows is not None:
            out_rows = _out_rows
        for t in tests:
            test_id = md5(t)
            groups = t.split(".")
            if len(groups) > 1:
                parent = ".".join(groups[:-1])
            elif t == "root":
                continue
            else:
                parent = "root"
            parent_id = md5(parent)
            out_rows.append((test_id, parent_id))
        if _out_rows is None:
            return out_rows


class ImportSql:
    def __init__(self, resultdir, start_time, end_time, url, build_name,
                 build_num, job, result_path, host='localhost', user='jenkins',
                 password=None, socket=None):
        warnings.filterwarnings('ignore', category=MySQLdb.Warning)
        self.host = host
        self.user = user
        self.socket = socket
        self.password = password
        self.resultdir = resultdir
        self.build_num = build_num
        self.build_name = build_name
        self.job = job
        self.job = self.job.replace("-", "_")
        self.job = self.job.replace(".", "_")
        self.url = url
        self.result_path = result_path
        if not start_time:
            start_time = 515462400
        self.build_start_time = datetime.datetime.fromtimestamp(
            int(start_time)).strftime('%Y-%m-%d %H:%M:%S')
        self.build_end_time = datetime.datetime.fromtimestamp(
            int(end_time)).strftime('%Y-%m-%d %H:%M:%S')

    def __enter__(self):
        self.connect()
        self.create_db()
        # Remove any duplicate build in the database before adding this build
        self.remove_build(self.build_num)
        return self

    def __exit__(self, type, value, traceback):
        self.cur.execute("update build set imported=TRUE "
                         "where build_id={}".format(self.build_num))
        self.db.commit()
        self.disconnect()

    def connect(self):
        if self.socket:
            self.db = MySQLdb.connect(user=self.user,
                                      unix_socket=self.socket,
                                      passwd=self.password,
                                      use_unicode=True, charset="utf8")
        else:
            self.db = MySQLdb.connect(host=self.host, user=self.user,
                                      passwd=self.password,
                                      use_unicode=True, charset="utf8")

        self.db.set_character_set('utf8')
        self.cur = self.db.cursor()
        self.cur.execute('SET NAMES utf8;')
        self.cur.execute('SET CHARACTER SET utf8;')
        self.cur.execute('SET character_set_connection=utf8;')

    def create_db(self):
        # '-' and '.' not allowed in database names
        self.cur.execute("create database if not exists {}".format(self.job))
        self.cur.execute("use {}".format(self.job))
        self.cur.execute("SET @@session.unique_checks = 0")
        self.cur.execute("SET @@session.foreign_key_checks = 0")
        self.db.commit()

        # build table
        self.cur.execute("create table if not exists build (" 
                         "build_id int not null,  "
                         "build_name char(100), "
                         "pass_count int, "
                         "fail_count int, "
                         "start_time datetime, "
                         "end_time datetime, "
                         "url char(128) not null, "
                         "filtered_pass_count int,"
                         "result_path varchar(1024), "
                         "imported bool, "
                         "primary key(build_id));")
        # test table
        self.cur.execute("create table if not exists test ("
                         "test_id char(32) not null, "
                         "test_name text(1024) not NULL, "
                         "primary key(test_id))")
        # parent table
        self.cur.execute("create table if not exists parent ("
                         "test_id char(32) not null, "
                         "parent_id char(32) not null, "
                         "primary key(test_id), "
                         "index parent_id(parent_id))")
        # result table
        self.cur.execute("create table if not exists result ("
                         "result_id int not null AUTO_INCREMENT,  "
                         "test_id char(32) not null, "
                         "arch enum('m32', 'm64') not null, "
                         "hardware char(32), "
                         "build_id int not null, "
                         "status enum('pass', 'fail', 'skip') not null, "
                         "filtered_status enum('pass', 'fail', 'skip') not null, "
                         "time decimal(32,6), "
                         "stdout text, "
                         "stderr text, "
                         "primary key(result_id), "
                         "index build_id(build_id), "
                         "index test_id(test_id), "
                         "index filtered_status(filtered_status), "
                         "index build_test_hw_arch_status(build_id, test_id, "
                         "hardware, arch, status))")
        # group_ table
        self.cur.execute("create table if not exists group_ ("
                         "build_id int not null,  "
                         "test_id char(32) not null, "
                         "pass_count int not null, "
                         "fail_count int not null, "
                         "filtered_pass_count int not null, "
                         "time decimal(32,6) not null, "
                         "primary key(build_id, test_id))")
        # artifact table
        self.cur.execute("create table if not exists artifact ("
                         "artifact_id int not null AUTO_INCREMENT, "
                         "type char(64) not null, "
                         "filename char(120) not null, "
                         "data longtext not null, "
                         "component_id int not null,"
                         "index component_id(component_id),"
                         "primary key(artifact_id))")
        # component table
        self.cur.execute("create table if not exists component ("
                         "component_id int not null AUTO_INCREMENT, "
                         "arch enum('m32', 'm64') not null, "
                         "status enum('success', 'failure', 'unstable', 'aborted') not null, "
                         "component_name char(100), "
                         "shard char(32), "
                         "machine char(64), "
                         "start_time datetime, "
                         "end_time datetime, "
                         "build int not null, "
                         "build_id int not null, "
                         "primary key(component_id))")
        # revision table
        self.cur.execute("create table if not exists revision ("
                         "revision_id int not null AUTO_INCREMENT, "
                         "project char(64) not null, "
                         "commit char(64) not null, "
                         "author char(64) not null, "
                         "description varchar(512) not null, "
                         "sha char(32) not null, "
                         "build_id int not null, "
                         "index build_id(build_id), "
                         "primary key(revision_id))")
        self.db.commit()

    def add_component(self, component):
        start_time = datetime.datetime.fromtimestamp(
            int(component['start_time'])).strftime('%Y-%m-%d %H:%M:%S')
        # note: end_time may be null if the component failed
        if component['end_time']:
            end_time = datetime.datetime.fromtimestamp(
                int(component['end_time'])).strftime('%Y-%m-%d %H:%M:%S')
        else:
            end_time = start_time
        if 'status' not in component or not component['status']:
            status = 'aborted'
        else:
            status = component['status']
        # These values are allowed to be null
        machine = component['machine'] if 'machine' in component else ''
        shard = component['shard'] if 'shard' in component else ''

        self.cur.execute("""insert into component (arch, component_name, """
                         """shard, machine, start_time, end_time, """
                         """build_id, build, status) values """
                         """({}, {}, {}, {}, {}, {}, {}, {}, {})"""
                         """""".format(q(component['arch']),
                                       q(component['name']),
                                       q(shard),
                                       q(machine), q(start_time),
                                       q(end_time), self.build_num,
                                       component['build'],
                                       q(status)))
        self.db.commit()
        self.cur.execute("select last_insert_id()")
        component_id = self.cur.fetchone()[0]
        if 'artifacts' in component:
            for artifact in component['artifacts']:
                if artifact['type'] == 'junit':
                    self.import_xml(self.resultdir + "/" + artifact['file'])
                else:
                    # note: currently only support importing text (not binary)
                    with open(self.resultdir + '/'
                              + artifact['file'], 'r') as f:
                        data = f.read()
                        data = q(data, escape=True)
                    self.cur.execute("""insert into artifact (type, """
                                     """filename, component_id, data) """
                                     """values ({}, {}, {}, {})"""
                                     """""".format(q(artifact['type']),
                                                   q(artifact['file']),
                                                   component_id, data))
                    self.db.commit()

    def remove_build(self, build_num):
        # Clean out results table
        self.cur.execute("""select count(*) from result where build_id="{}" """.format(build_num))
        count = int(self.cur.fetchone()[0])
        while count > 0:
            print("deleting results from matching build: " + str(count))
            self.cur.execute("delete quick from result where build_id={} limit 100000".format(build_num))
            self.db.commit()
            self.cur.execute("""select count(*) from result where build_id="{}" """.format(build_num))
            count = int(self.cur.fetchone()[0])
        # Clean out group_ table
        self.cur.execute("""select count(*) from group_ where build_id="{}" """.format(build_num))
        count = int(self.cur.fetchone()[0])
        while count > 0:
            print("deleting group stats from matching build: " + str(count))
            self.cur.execute("delete quick from group_ where build_id={} limit 100000".format(build_num))
            self.db.commit()
            self.cur.execute("""select count(*) from group_ where build_id="{}" """.format(build_num))
            count = int(self.cur.fetchone()[0])
        # Clean out components table
        self.cur.execute("""select component_id from component where build_id="{}" """.format(build_num))
        for component_id in self.cur.fetchall():
            # Clean out artifacts table
            self.cur.execute("delete quick from artifact where "
                             "component_id=%s", [component_id])
            self.db.commit()
        self.cur.execute("""select count(*) from component where build_id="{}" """.format(build_num))
        count = int(self.cur.fetchone()[0])
        print("deleting components from matching build: " + str(count))
        self.cur.execute("delete quick from component where build_id={} limit 100000".format(build_num))
        self.db.commit()
        print("deleting revisions for build: {}".format(build_num))
        self.cur.execute("""delete quick from revision where build_id="{}" """.format(build_num))
        self.db.commit()
        print("deleting build: {}".format(build_num))
        self.cur.execute("""delete quick from build where build_id="{}" """.format(build_num))
        self.db.commit()

    def prune(self):
        self.cur.execute("select count(*) from build")
        count = int(self.cur.fetchone()[0])
        if self.job in max_builds:
            count_max = max_builds[self.job]
        else:
            count_max = max_builds['default']
        if count <= count_max:
            return
        limit = count - count_max
        # Don't prune more than 3 builds at a time, to help with import latency
        if limit > 3:
            limit = 3
        self.cur.execute("select build_id from build order by build_id asc "
                         + "limit {}".format(limit))
        for prune_build in self.cur.fetchall():
            self.remove_build(prune_build[0])

    def import_xml(self, xml_file):
        print("importing file: %s" % xml_file)
        tt = TestTree()
        try:
            tt.parse(xml_file)
        except SyntaxError:
            # test does not have valid junit
            print("INFO: junit file is not valid, skipping")
            return

        new_tests = tt.new_tests(self.cur)
        if new_tests:
            print("found {} new tests".format(str(len(new_tests))))
            test_rows = tt.test_rows(new_tests)
            values = ["({}, {})".format(q(id), q(name)) for (id, name) in test_rows]
            self.cur.execute("""insert ignore into test (test_id, test_name) values """ +
                             ", ".join(values))

            parent_rows = tt.parent_rows(new_tests)
            values = ["({}, {})".format(q(id), q(parent)) for (id, parent) in parent_rows]
            self.cur.execute("insert ignore into parent "
                             "(test_id, parent_id) values " +
                             ", ".join(values))
            self.db.commit()

        print("updating group statistics")
        groups = tt.group_rows()
        values = ["({}, {}, {}, {}, {}, {})".format(self.build_num,
                                                    q(test_id),
                                                    pass_count,
                                                    fail_count,
                                                    filtered_pass_count,
                                                    time)
                  for (test_id, pass_count, fail_count, filtered_pass_count, time) in groups]
        self.cur.execute("insert into group_ ("
                         "build_id, test_id, pass_count, "
                         "fail_count, filtered_pass_count, time) "
                         "values " +
                         ", ".join(values) +
                         " on duplicate key update "
                         "pass_count = pass_count + values(pass_count), "
                         "fail_count = fail_count + values(fail_count), "
                         "filtered_pass_count = filtered_pass_count + "
                         "values(filtered_pass_count), "
                         "time = time + values(time)")
        self.db.commit()

        print("updating test results")
        results = tt.result_rows()
        if not results:
            # junit is empty, so these results will be ignored
            print("WARN: Empty junit: {}".format(xml_file))
            return
        values = ["({}, {}, {}, {}, {}, {}, {}, {}, {})".format(q(test_id), q(hardware), q(arch), self.build_num,
                                                                q(status), q(filtered_status), time,
                                                                q(stdout, True), q(stderr, True))
                  for (test_id, hardware, arch, status, filtered_status, time, stdout, stderr) in results]
        self.cur.execute("insert ignore into result ("
                         "test_id, hardware, arch, build_id, status, "
                         "filtered_status, time, stdout, stderr) values " +
                         ", ".join(values))
        self.db.commit()

        print("updating build statistics")
        self.cur.execute("select pass_count, filtered_pass_count, fail_count "
                         "from group_ where build_id={} and test_id={}".format(self.build_num,
                                                                               q(md5('root'))))
        (pass_count, filtered_pass_count, fail_count) = self.cur.fetchone()

        self.cur.execute("""replace into build(build_id, build_name, """
                         """pass_count, filtered_pass_count, fail_count, """
                         """start_time, end_time, url, """
                         """result_path, imported) """
                         """values({}, {}, {}, {}, {}, {}, {}, {}, {}, {})""".format(self.build_num, q(self.build_name),
                                                                                     pass_count, filtered_pass_count,
                                                                                     fail_count,
                                                                                     q(self.build_start_time),
                                                                                     q(self.build_end_time),
                                                                                     q(self.url),
                                                                                     q(self.result_path), 'FALSE'))
        self.db.commit()
        print("import done")

    def disconnect(self):
        self.db.close()

    def add_revisions(self, revisions):
        print("adding revisions for build")
        for project in revisions:
            details = revisions[project]
            self.cur.execute("""insert into revision(project, """
                             """commit, author, description, """
                             """sha, build_id) values ({}, {}, {}, {}, {}, """
                             """{})""".format(q(project), q(details['commit']),
                                              q(details['author'].encode('utf-8'), True),
                                              q(details['description'].encode('utf-8'), True),
                                              q(details['sha']),
                                              self.build_num))
            self.db.commit()


def main():
    results_path = '/tmp/mesa_ci_results'
    if not os.path.exists(results_path):
        os.mkdir(results_path)

    sql_host = 'localhost'
    if "SQL_DATABASE_HOST" in os.environ:
        sql_host = os.environ["SQL_DATABASE_HOST"]

    sql_user = 'jenkins'
    if "SQL_DATABASE_USER" in os.environ:
        sql_user = os.environ["SQL_DATABASE_USER"]

    sql_socket = None
    if "SQL_DATABASE_SOCK" in os.environ:
        sql_socket = os.environ["SQL_DATABASE_SOCK"]

    results_importer = ResultsImporter(results_path, sql_host=sql_host,
                                       sql_user=sql_user,
                                       sql_socket=sql_socket)
    # consume any existing tar files
    for tarfile in glob.glob(results_path + "/*.xz"):
        results_importer.consume(tarfile)
    while True:
        tarfile = results_importer.poll()
        results_importer.consume(tarfile)


if __name__ == "__main__":
    main()
